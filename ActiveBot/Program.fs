﻿open System.Net
open System.IO
open ActiveBot.JsonSerialize;
open ActiveBot.Base;


let messageInitial messages =
    messages |> List.find (fun x -> match x with | Some x -> true | None -> false)


let initialListener =
    let listener = new HttpListener();
    listener.Prefixes.Add "http://localhost:5000/";
    listener.Start();
    listener;

[<EntryPoint>]
let main argv =
 //    dowloadAllJson() |> ignore;
    let listener = initialListener;
    while true do
        let result = async {
           let! context = listener.GetContextAsync() |> Async.AwaitTask;
           let stream = new StreamReader(context.Request.InputStream)
           let! body = stream.ReadToEndAsync() |> Async.AwaitTask;
           printf "%s" body;
           let m = updateJsonProvider.Parse(body)
           let message = match (m.Message, m.EditedMessage) with
                | (Some s, _) -> OriginalMessage s
                | (_, Some ss) -> EditedMessage ss
           let next = (fun x -> async { return Some x })
           routePipeLine next message |> Async.RunSynchronously |> ignore
           context.Response.OutputStream.Close();
        }
        let ans = result |> Async.RunSynchronously
        ans |> ignore;
    0







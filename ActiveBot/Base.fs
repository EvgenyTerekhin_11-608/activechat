module ActiveBot.Base
open FSharp.Data

type updateJsonProvider = JsonProvider<"./jsons/webHookUpdate.json", SampleIsList=true>

type Status<'a> =
    | Success of 'a
    | Error of string

let stringAggregate (messages: string list) = (messages |> Seq.map (fun x -> sprintf "%s\n" x) |> Seq.concat).ToString();

let unwrap o1 sF fF =
    match o1 with
    | Success s -> sF s
    | Error e -> fF e;

let unwrap2 (o1, o2) sF fF =
    match (o1, o2) with
    | (Success s1, Success s2) -> sF (s1, s2)
    | (Error e1, Error e2) -> fF (stringAggregate [ e1; e2 ]);
    | (Error e11, _) -> fF (stringAggregate [ e11 ]);
    | (_, Error e2) -> fF (stringAggregate [ e2 ]);

let unwrap3 o1 o2 o3 sF fF =
    match (o1, o2, o3) with
    | (Success s1, Success s2, Success s3) -> sF stringAggregate s1 s2 s3
    | (Error e1, Error e2, Error e3) -> fF (stringAggregate [ e1; e2; e3 ]);
    | (_, Error e2, Error e3) -> fF (stringAggregate [ e2; e3 ]);
    | (Error e2, Error e3, _) -> fF (stringAggregate [ e2; e3 ]);
    | (Error e1, _, Error e3) -> fF (stringAggregate [ e1; e3 ]);
    | (Error e1, _, _) -> fF (stringAggregate [ e1; ]);
    | (_, Error e2, _) -> fF (stringAggregate [ e2; ]);
    | (_, _, Error e3) -> fF (stringAggregate [ e3 ]);


type Message =
             | OriginalMessage of updateJsonProvider.Message
             | EditedMessage of updateJsonProvider.EditedMessage

type httpFunc = Message -> Async<Message option>

type HttpHandler = httpFunc -> httpFunc


let (>=>) (handler1: HttpHandler) (handler2: HttpHandler): HttpHandler =
    let func next str = (handler2 next |> handler1) str;
    func

let rec choose (handlers: HttpHandler list): HttpHandler =
    let func next ctx =
            match handlers with
            | head :: tail ->
                          let res = head next ctx |> Async.RunSynchronously
                          match res with
                            | Some _ -> async { return res }
                            | _ -> choose tail next ctx
            | [] -> async { return None }
    func





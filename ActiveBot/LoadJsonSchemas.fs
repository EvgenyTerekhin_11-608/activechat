module ActiveBot.WebHookRequests
open System.Net
open System.Net.Http
open ActiveBot.Data;

type json = string
type filename = string

let toPipe1 func x y = func (x, y);

[<Literal>]
let TelegramUrl = "https://api.telegram.org/"

let telegramActionUrl token action = TelegramUrl + ("bot" + token + "/" + action);

let httpClientInitial =
    let proxy = new WebProxy("185.17.123.46:65233")
    proxy.Credentials <- (new NetworkCredential("pesoshin", "L0a8ReO"))
    let httpClientHandler = new HttpClientHandler()
    httpClientHandler.Proxy <- proxy;
    let client = new HttpClient(httpClientHandler)
    client;

let getString (client: HttpClient) (url: string) =
    async {
            use! response = client.GetAsync(url) |> Async.AwaitTask
            let! str = response.Content.ReadAsStringAsync() |> Async.AwaitTask
            return str
            }

let getStringHttpInitialed = getString httpClientInitial;

let commonPoint token (actionName: string) (param: (string * string) seq) =
    let queryString = param |> Seq.map (fun (key, value) -> sprintf "%s=%s" key value) |> String.concat "&";
    let url = sprintf "%s?%s" (telegramActionUrl token actionName) queryString;
    getStringHttpInitialed url;

let sendMessage token chatid messageText = commonPoint token "sendMessage" [ ("chat_id", chatid); ("text", messageText) ]


let getUpdates token = commonPoint token "getUpdates" [];

let deleteWebhook token = commonPoint token "deleteWebhook" [];

let getWebHookInfo token = commonPoint token "getWebhookInfo" [];

let setWebhook webHookUrl token = commonPoint token "setWebhook" [ ("url", webHookUrl) ];

let sendMessageWithToken = sendMessage telegramToken

let addFileInfo (httpFunc: string -> Async<json>) (filename: filename) token =
    async {
        let! js = httpFunc token
        return (filename, js);
    }

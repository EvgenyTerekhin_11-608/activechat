module ActiveBot.JsonSerialize
open ActiveBot.Base
open ActiveBot.WebHookRequests

open ActiveBot.WebHookRequests;

let (|>>) obj func =
    match obj with
    | Some s -> func s
    | None -> None
    
let (|>>>) (obj1, obj2) func =
    match (obj1, obj2) with
    | (Some s1, Some s2) -> func s1 s2
    | _ -> None

let messageBind obj (func: updateJsonProvider.Message -> Async<Message option>) =
    match obj with
    | OriginalMessage m -> func m
    | _ -> async { return None }

let editMessageBind obj (func: updateJsonProvider.EditedMessage -> Async<Message option>) =
    match obj with
    | EditedMessage m -> func m
    | _ -> async { return None }


let sendMessageHandler: HttpHandler =
    let func (_: httpFunc) (json: Message) =
                (json |> messageBind)
                    (fun m ->
                            async {
                                let! _ = sendMessageWithToken (m.Chat.Id.ToString()) m.Text
                                return Some(OriginalMessage m);
                            })
    func

let contains (str: string): HttpHandler =
    let func next message =
        (message |> messageBind) (fun x -> if x.Text.Contains(str) then next message else async { return None })
    func

let isAuth: HttpHandler =
    let func next message =
        (message |> messageBind) (fun x -> if (x.From.Id % 2 = 1) then next message else async { return None });
    func

let isOriginalMessageHandler: HttpHandler =
    let func next message = match message with
    |OriginalMessage _ -> next message
    | _ -> async { return None }
    func

let routePipeLine =
          choose
             [
                isOriginalMessageHandler >=> choose [
                    contains "/start" >=> isAuth
                ] >=> sendMessageHandler
             ]
